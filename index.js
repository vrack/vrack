const Loader = require('vrack-loader')
const fs = require('fs')
const path = require('path')
const { isMainThread, workerData } = require('worker_threads')

var fileConfig = './config.vrack.json'
if (isMainThread && process.argv.length === 3) fileConfig = process.argv[process.argv.length - 1]
else if (!isMainThread) fileConfig = workerData.fileConfig
const config = JSON.parse(fs.readFileSync(path.join(fileConfig)))
Loader.init()
Loader.process(config)
