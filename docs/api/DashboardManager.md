# DashboardManager

Дашборд-менеджер отвечает за управление (запуск, остановку) дашбордов,
получение и обновление списка всех дашбордов, за работу с ошибками, структурой
и взаимодействие с устройствами дашборда.

### Критические ошибки

Критические ошибки дашборда хранятся в памяти VRack, пока сервис не будет выключен
или список ошибок не будет принудительно очищен командой [dashboardErrorsClear](#dashboarderrorsclear).

- LEVEL 1 [dashboardRun](#dashboardrun) - Запуск дашборда
- LEVEL 1 [dashboardStop](#dashboardstop) - Остановка дашборда
- LEVEL 1 [dashboardErrors](#dashboarderrors) - Получение критических ошибок
- LEVEL 1 [dashboardErrorsClear](#dashboarderrorsclear) - Удаление всех критических ошибок
- LEVEL 1 [dashboardListUpdate](#dashboardlistupdate) - Обновление списка дашбордов
- LEVEL 3 [dashboard](#dashboard) - Получение данных дашборда
- LEVEL 3 [dashboardList](#dashboardlist) - Получение списка дашбордов
- LEVEL 3 [dashboardMeta](#dashboardmeta) - Получение мета-данных дашборда

## dashboardRun

Запуск дашборда

### Параметры

- **dashboard** _string_ - Идентификатор дашборда

### Исключения

- **Error** - Dashboard not found
- **Error** - Dashboard is already running

## dashboardStop

Остановка дашборда

### Параметры

- **dashboard** _string_ - Идентификатор дашборда

### Исключения

- **Error** - Dashboard not found
- **Error** - Dashboard not run

## dashboardErrors

Получение критических ошибок

### Параметры

- **dashboard** _string_ - Идентификатор дашборда

### Возвращает

Массив ошибок, где:

- **name** _string_ - Название ошибки (Error/DashboardError/DeviceError)
- **action** _string_ - Глобальное действие, при котором произошла ошибка (init/preprocess/process)
- **stack** _string_ - Stack trace ошибки
- **id** _integer_ - Идентификатор ошибки в этой сессии
- **threadId** _integer_ - Идентификатор процесса
- **startedAt** _integer_ - Timestamp начала работы дашборда
- **endedAt** _integer_ - Timestamp окончания работы дашборда

```json
{
 [
    {
      "name": "DashboardError",
      "action": "init",
      "stack": "DashboardError: Device type 'basic.Interval1' not found...",
      "id": 1000,
      "threadId": 2,
      "startedAt": 1616328678013,
      "endedAt": 1616328678108
    }
  ]

```

### Исключения

- **Error** - Dashboard not found

## dashboardErrorsClear

Удаление всех критических ошибок

### Параметры

- **dashboard** _string_ - Идентификатор дашборда

### Исключения

- **Error** - Dashboard not found

## dashboardListUpdate

Обновление списка дашбордов

### Возвращает

Действительный список дашбордов, где:

- **id** _string_ - Идентификатор дашборда
- **name** _string_ - Читаемое название дашборда
- **group** _string_ - Группа
- **run** _boolean_ - Запущен ли дашборд true/false (да/нет)
- **path** _string_ - Путь до файла дашборда
- **structurePath** _string_ - Путь до файла структуры дашборда
- **metaPath** _string_ - Путь до файла мета-данных дашборда
- **errors** _integer_ - Количество ошибок
- **deleted** _boolean_ - Удален ли дашборд true/false (да/нет)

```json
{
  "test": {
    "id": "test",
    "name": "No name",
    "group": "Default",
    "run": false,
    "path": "dashboards/test.json",
    "structurePath": "dashboards/test.struct.json",
    "metaPath": "dashboards/test.meta.json",
    "errors": 1,
    "deleted": false
  }
}
```

### Исключения

- **Error** - Dashboards dir not found

## dashboard

Получение данных дашборда, см. [dashboardListUpdate](#dashboardlistupdate)

### Параметры

- **dashboard** _string_ - Идентификатор дашборда

### Возвращает

```json
{
  "id": "test",
  "name": "No name",
  "group": "Default",
  "run": false,
  "path": "dashboards/test.json",
  "structurePath": "dashboards/test.struct.json",
  "metaPath": "dashboards/test.meta.json",
  "errors": 1,
  "deleted": false
}
```

### Исключения

- **Error** - Dashboard not found

## dashboardList

Получение списка дашбордов

### Возвращает

Действительный список дашбордов, см. [dashboardListUpdate](#dashboardlistupdate)

```json
{
  "test": {
    "id": "test",
    "name": "No name",
    "group": "Default",
    "run": false,
    "path": "dashboards/test.json",
    "structurePath": "dashboards/test.struct.json",
    "metaPath": "dashboards/test.meta.json",
    "errors": 1,
    "deleted": false
  }
}
```

## dashboardMeta

Получение мета-данных дашборда

### Параметры

- **dashboard** _string_ - Идентификатор дашборда

### Возвращает

Мета-данные дашборда, где:

- **name** _string_ - Читаемое название дашборда
- **group** _string_ - Группа
- **description** _string_ - Описание дашборда
- **private** _boolean_ - Флаг приватности
- **autoStart** _boolean_ - Флаг автоматического запуска после старта VRack
- **autoReload** _boolean_ - Флаг автоматического перезапуска при возникновении критической ошибки
- **version** _string_ - Версия дашборда

```json
{
  "name": "No name",
  "group": "Default",
  "description": "Dashboard description",
  "private": false,
  "autoStart": true,
  "autoReload": true,
  "version": "1.0"
}
```

### Исключения

- **Error** - Dashboard not found
