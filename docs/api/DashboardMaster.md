# DashboardMaster

Дашборд-мастер предоставляет команды относящиеся к конкретному работающему дашборду, ни одна из 
команд дашборд-мастера не будет работать, если дашборд не был запущен.

- LEVEL 1 [dashboardStructureUpdate](#dashboardstructureupdate) - Отправка новой структуры
- LEVEL 1 [dashboardDevicePush](#dashboarddevicepush) - Отправка данных в порт устройства
- LEVEL 2 [dashboardDeviceAction](#dashboarddeviceaction) - Отправка `action` запроса устройству
- LEVEL 2 [dashboardDeviceOutputListen](#dashboarddeviceoutputlisten) - Захват выхода устройства
- LEVEL 3 [dashboardShares](#dashboardshares) - Запрос на преждевременную отправку `shares` данных дашборда
- LEVEL 3 [dashboardStructure](#dashboardstructure) - Получение структуры дашборда

## dashboardStructureUpdate

Отправка новой структуры. Саму структуру дашборда можно получить используя команду
[dashboardStructure](#dashboardstructure).

### Параметры

- **dashboard** _string_ - Идентификатор дашборда
- **structure** _object_ - Структура дашборда

### Возвращает

Обновленную структуру дашборда

### Исключения

- **Error** - Dashboard not found
- **Error** - Dashboard not run

## dashboardDevicePush

Отправка данных в порт устройства

### Параметры

- **dashboard** _string_ - Идентификатор дашборда
- **device** _string_ - Идентификатор устройства
- **port** _string_ - Action устройства
- **push** _object_ - Данные для отправки устройству

### Возвращает

Результат выполнения input handler устройства

### Исключения

- **Error** - Dashboard not found
- **Error** - Dashboard not run
- **Error** - Device not found
- **Error** - Action on device not found



## dashboardDeviceAction

Отправка action запроса устройству

### Параметры

- **dashboard** _string_ - Идентификатор дашборда
- **device** _string_ - Идентификатор устройства
- **action** _string_ - Action устройства
- **push** _object_ - Данные для отправки устройству

### Возвращает

Результат выполнения `action` устройства

### Исключения

- **Error** - Dashboard not found
- **Error** - Dashboard not run
- **Error** - Device not found
- **Error** - Action on device not found



## dashboardDeviceOutputListen

Захват выхода устройства

### Параметры

- **dashboard** _string_ - Идентификатор дашборда
- **device** _string_ - Идентификатор устройства
- **port** _string_ - Идентификатор порта
- **timeout** _object_ - Таймаут захвата порта

### Возвращает

Захваченый и обработный результат отправленного на выход устройства значения

### Исключения

- **Error** - Dashboard not found
- **Error** - Dashboard not run
- **Error** - Device not found
- **Error** - Port not found
- **Error** - Action on device not found

## dashboardShares

Запрос на преждевременную отправку `shares` данных дашборда

### Параметры

- **dashboard** _string_ - Идентификатор дашборда

### Исключения

- **Error** - Dashboard not found

## dashboardStructure

Получение структуры дашборда

### Параметры

- **dashboard** _string_ - Идентификатор дашборда

### Возвращает

Структуру дашборда в виде ассоциативного списка, где:

- **id** _string_ - Идентификатор устройства
- **type** _string_ - Тип устройства
- **outputs** _object_ - Описание выходов
- **inputs** _object_ - Описание входов
- **settings** _object_ - Индивидуальные настройки устройства
- **actions** _object_ - Список `action` устройства
- **display** _object_ - Настройки отображения устройства

```json
{
  "ErrorManager": {
    "id": "ErrorManager",
    "type": "dispatch.EventManager",
    "outputs": { "registered": null, "updated": [{ "device": "ClickHouseAdapter", "port": "event" }] },
    "inputs": { "event": { "device": "EventTCPRec", "port": "object" } },
    "settings": { "message": true, "shares": true, "storage": true },
    "actions": { "get.history": [] },
    "display": {
      "header_bg": "#3d3835",
      "body_bg": "#45423d",
      "group_bg": "#202020",
      "is_rotated": false,
      "row": 1,
      "col": 8
    }
  },
}
```

### Исключения

- **Error** - Dashboard not found
