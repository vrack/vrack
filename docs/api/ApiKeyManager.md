# ApiKeyManager

Менеджер ключей доступа. Хранит и обрабатывает
добавление/удаление ключей доступа. После создания
ключа доступа, он не может быть изменен.
Нельзя задать сам ключ или приватный ключ, они генерируются
автоматически случайным образом нужной длины.
Ключ доступа **по умолчанию** `default`, если VRack работает локально,
можно оставить ключ доступа по умолчанию для удобства,
иначе рекомендуется создать новый и удалить ключ по умолчанию.

Ключи доступа разделяются на 3 уровня:

- **1** Администратор - Полное управление
- **2** Оператор - Только просмотр и отправка Action в устройства
- **3** Диспетчер - Только просмотр
- **1000** Гостевой уровень доступа

Менеджер осуществляет авторизацию клиентов `/src/security/Guard`,
за авторизацию отвечает команда `apiKeyAuth`.

- LEVEL 1 [apiKeyList](#apikeylist) - Возвращает список действующих ключей
- LEVEL 1 [apiKeyAdd](#apikeyadd) - Создание нового ключа
- LEVEL 1 [apiKeyUpdate](#apikeyupdate) - Обновление названия ключа
- LEVEL 1 [apiKeyUpdateFilter](#apikeyupdatefilter) - Обновление фильтра ключа
- LEVEL 1 [apiKeyRegisteredFilters](#apikeyregisteredfilters) - Получение списка зарегистрированных (доступных для обработки) фильтров
- LEVEL 1 [apiKeyDelete](#apikeydelete) - Удаление ключа
- LEVEL 1 [apiKeyIpAdd](#apikeyipadd) - Добавление подсети для ключа
- LEVEL 1 [apiKeyIpDelete](#apikeyipdelete) - Удаление подсети для ключа
- LEVEL 3 [apiKeyFilters](#apikeyfilters) - Возвращает список фильтров для данного ключа
- LEVEL 1000 [apiKeyAuth](#apikeyauth) - Авторизация клиента VRack
- LEVEL 1000 [apiPrivateAuth](#apiprivateauth) - Авторизация клиента VRack с приватным ключом

## apiKeyList

Возвращает список действующих ключей

### Возвращает

```js
[
  {
    name: 'test key',                             // Именование ключа
    description: 'test description'               // Описание ключа
    level: 1,                                     // Уровень 1,2,3
    key: "1234567890123456",                      // Ключ доступа
    private: "12345678901234567890123456789012",  // Приватный ключ
  },
  //...
];
```

## apiKeyAdd

Создание нового ключа

### Параметры

- **name** _string_ - Название ключа
- **description** _string_ - Описание
- **level** _integer_ - Уровень доступа
- **cipher** _boolean_ - Использовать шифрование

```js
{
  name: 'test key',
  description: 'test description'
  level: 1,
  cipher: true
}
```

### Возвращает

Новый созданный ключ

```js
{
  name: 'test key',
  description: 'test description'
  level: 1,
  key: "1234567890123456",
  private: "12345678901234567890123456789012",
}
```

## apiKeyUpdateFilter

Обновление фильтра

### Параметры

- **key** _string_ - Ключ
- **path** _string_ - Путь фильтра/название
- **list** _array_ - Список фильтра

```js
{
  key: "Default",
  path: "dashboards",
  list: ['accessed-id-dashboard']
}
```

### Исключения:

- **Error** - Key not found
- **Error** - Filter not registered
- **Error** - Incorrect filter element

## apiKeyRegisteredFilters

Получение списка зарегистрированных фильтров

### Возвращает

Список зарегистрированных фильтров

```js
[
    {
      name: 'Api commands filter',
      description: 'Фильтрует доступ к конкретным командам API',
      path: 'commands'
    },
    {
      name: 'Dashboards filter',
      description: 'Фильтрует доступ к конкретным дашбордам',
      path: 'dashboards'
    }
  ]
```

## apiKeyRegisteredFilters

Получение списка зарегистрированных фильтров

### Возвращает

Список зарегистрированных фильтров

```js
[
    {
      name: 'Api commands filter',
      description: 'Фильтрует доступ к конкретным командам API',
      path: 'commands'
    },
    {
      name: 'Dashboards filter',
      description: 'Фильтрует доступ к конкретным дашбордам',
      path: 'dashboards'
    }
  ]
```

## apiKeyDelete

Удаление ключа

### Параметры

- **key** _string_ - ключ

```js
{
  key: "1234567890123456",
}
```

### Исключения:

- **Error** - Key not found

## apiKeyIpAdd

Добавление подсети для ключа

### Параметры

- **key** _string_ - ключ
- **network** _string_ - Подсеть ipv4,ipv6 для доступа по ключу ('127.0.0.1/24'...)

```js
{
  key: "1234567890123456";
  ip: "127.0.0.1/24";
}
```
### Исключения:

- **Error** - Key not found
- **Error** - Incorrect IP network

## apiKeyIpDelete

Удаление подсети для ключа

### Параметры

- **key** _string_ - ключ
- **network** _string_ - Подсеть для удаления

```js
{
  key: "1234567890123456";
  ip: "127.0.0.1/24";
}
```
### Исключения:

- **Error** - Key not found
- **Error** - IP network not found

## apiKeyFilters

Получения фильтров текущего ключа

### Возвращает

filter.path: ['filterArraylist']

```js
{ dashboards: [ 'test' ] }
```

## apiKeyAuth

Авторизация клиента VRack

Проводит первичную авторизацию, если ключ доступа имеет приватный ключ шифрования,
формирует случайную строку верификации, которую должен зашифровать ключом шифрования
клиент и отправить команде `apiPrivateAuth`.

Параметры:

- **key** _string_ - ключ

### Возвращает

Если нет приватного ключа:

```js
{
  level: 1,
  cipher: false
}
```

Если требуется шифрование:

```js
{
  verify: '1234567890123456',
  cipher: true;
}
```

### Исключения

- **Error** - Key not found
- ****
## apiPrivateAuth

Авторизация клиента VRack с приватным ключом см. [apiKeyAuth](#apikeyauth)

### Параметры

- **verify** _string_ - Шифрованная строка верификации

### Возвращает

```js
{
  level: 1,
  cipher: true
}
```

### Исключения

- **Error** - Invalid private key
