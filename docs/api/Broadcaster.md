# Broadcaster

Бродкастер подписывает клиентов VRack на специальные внутренние каналы для получения данных в режиме онлайн.

- LEVEL 3 [channelJoin](#channeljoin) - Подключение к каналу
- LEVEL 3 [channelLeave](#channelleave) - Отключение от канала
- LEVEL 3 [channelLeaveAll](#channelleaveall) - Отключение от всех каналов
- LEVEL 3 [channelsLeave](#channelsleave) - Отключение от нескольких каналов

## channelJoin

Подключение к каналу

Можно использовать параметр `filter` для подключения к нескольким подканалам. Такой подход можно использовать только в редких случаях.

### Параметры

- **channel** _string_ - Канал
- **filter** _array_ - Список подканалов

```js
{
  channel: 'dashboards.test.Memory',
  filter: ['terminal','render','error']
}
```

### Возвращает

Список подключенных каналов

```json
[
  "dashboards.test.Memory.terminal",
  "dashboards.test.Memory.render",
  "dashboards.test.Memory.error"
]
```

## channelLeave

Отключение от канала

### Параметры

- **channel** _string_ - Канал


## channelLeaveAll

Отключение от всех каналов

### Возвращает

Список отключенных каналов

```json
[
  "dashboards.test.Memory.terminal",
  "dashboards.test.Memory.render",
  "dashboards.test.Memory.error"
]
```

## channelsLeave

Отключение от нескольких каналов

### Параметры

- **channels** _array_ - Список каналов на отключение

```json
[
  "dashboards.test.Memory.terminal",
  "dashboards.test.Memory.render",
  "dashboards.test.Memory.error"
]
```
