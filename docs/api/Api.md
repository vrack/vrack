# API

Работа с VRack API предоставляется любому из зарегистрированных провайдеров данных,
по умолчанию это всегда **WebSocketServer**. Может быть написан любой другой провайдер,
способный предоставить уникальный идентификатор для каждого клиента VRack.

Работа с API:

1.  [С чего начать](#с-чего-начать)
2.  [Авторизация](#авторизация)
3.  [Broadcast](#broadcast)
4.  [Дополнительно](#дополнительно)

API для работы с VRack:

- [ApiKeyManager](/docs/api/ApiKeyManager.md) - Работа с ключами доступа, авторизация
- [DashboardManager](/docs/api/DashboardManager.md) - Управление дашбордами
- [Broadcaster](/docs/api/Broadcaster.md) - Работа с системой бродкастов
- [DeviceInformation](/docs/api/DeviceInformation.md) - Получения информации об устройствах

## С чего начать

Формат работы с API - JSON. Серверу отправляется набор данных, сервер их обрабатывает, дополняет и возвращает.

Обязательные поля для отправки запроса:

- **command** _string_ - Команда
- **data** _object_ - Объект с параметрами

Самый простой запрос может выглядеть так:

```json
{
  "command": "apiKeyAuth",
  "data": {
    "key": "default"
  }
}
```

Пример ответа:

```json
{
  "command": "apiKeyAuth",
  "data": {
    "key": "default"
  },
  "providerID": "WebSocketServer",
  "clientID": 258,
  "result": "success",
  "resultData": { "level": 1, "cipher": false }
}
```

Обязательные поля ответа:

- **providerID** _string_ - Провайдер, через который поступил запрос
- **clientID** _integer_ - Уникальный идентификатор соединения
- **result** _string_ - Результат выполнения запроса success/error
- **resultData** _object_ - Данные результата выполнения

Нет необходимости пытаться изменить `providerID` или `clientID`, эти данные заполняются на этапе приема данных, чтобы определить кому должен быть отправлен ответ.

Сервер работает асинхронно, и практика отправки одного запроса, и ожидания ответа на него - плохая. Чтобы корректно выстроить последовательность запроса и ответа, необходимо каждому запросу назначать уникальный идентификатор, который формируется на стороне клиента.

```json
{
  "index": 1001,
  "command": "apiKeyAuth",
  "data": {
    "key": "default"
  }
}
```

Ответ будет содержать запрос, поэтому, при получении ответа, по индексу можно определить, к какому запросу он относится.

```json
{
  "index": 1001,
  "command": "apiKeyAuth",
  "data": {
    "key": "default"
  },
  "providerID": "WebSocketServer",
  "clientID": 258,
  "result": "success",
  "resultData": { "level": 1, "cipher": false }
}
```

В JS можно использовать простые очереди с `Promise`

```js
class VRack {
  // Объект очереди
  queue = new Map();

  // Инкрементный идентификатор запроса
  index = 1000;

  /**
   * Хелпер для реализации отправки запроса
   *
   * @param {String} command Команда
   * @param {Object} data  Объект с параметрами
   * @param {Function} resolve Callback успешного выполнения запроса
   * @param {Function} reject Callback ошибки запроса
   */
  command(command, data, resolve, reject) {
    data.command = command;
    data.index = this.index;
    this.index++; // Изменяем индекс, чтобы он всегда был уникальным для запроса
    this.queue.set(params.index, { resolve: resolve, reject: reject });
    // ... далее отправка запроса, установка таймаутов и т.п.
  }

  /**
   * Функция, которая вызывается, когда получен ответ от сервера
   *
   * @param {Object} data данные ответа от сервера
   */
  response(data) {
    // Смотрим наличие индекса
    if (data.index) {
      // Есть ли он на очереди
      if (this.queue.has(data.index)) {
        var func = this.queue.get(data.index);
        if (data.result === "error") {
          // Если вернулся ответ с ошибкой, вызываем reject
          func.reject(new Error(data.resultData));
        } else {
          // Если все ок, вызываем resolve
          func.resolve(data);
        }
        this.queue.delete(data.index);
      }
    }
  }

  /**
   * Простая обертка, чтобы использовать полноценно Promise для
   * отправки запросов
   *
   * @param {String} command Команда
   * @param {Object} data  Объект с параметрами
   *
   */
  commandPromise(command, data) {
    return new Promise((resolve, reject) => {
      this.command(command, data, resolve, reject);
    });
  }
}
```

Пример выше является просто демонстрацией идеи использования индекса для определения принадлежности ответа к запросу. Реальный пример кода будет отличаться, например, наличием таймаутов на отправку запроса.

Необходимо учитывать, что **нельзя использовать параметр `__index` для указания индекса запроса**. Этот параметр зарезервирован для общения между дашборд-менеджером и дашбордом.

## Авторизация

Изначально провайдер выставляет каждому клиенту VRack гостевой уровень доступа (1000).
Единственные доступные команды для этого уровня - команды авторизации.

Если не требуется шифрование, достаточно одного ключа доступа и одной команды авторизации.

Пример запроса авторизации с ключом по умолчанию:

```json
{
  "command": "apiKeyAuth",
  "data": {
    "key": "default"
  }
}
```

Ответ в случае успешной авторизации:

```json
{
  "index": 1001,
  "command": "apiKeyAuth",
  "data": {
    "key": "default"
  },
  "providerID": "WebSocketServer",
  "clientID": 258,
  "result": "success",
  "resultData": { "level": 1, "cipher": false }
}
```

После чего для данного соединения будут доступны все команды соответсвующего уровня или больше.

Для авторизации с шифрованием необходим еще приватный ключ шифрования. Нужно учитывать, что **в данный момент
шифрование в VRack условное, и, вероятнее всего, не обеспечивает 100% безопасность**.

После отправки запроса с ключом доступа сервер должен ответить:

```json
{
  "index": 1001,
  "command": "apiKeyAuth",
  "data": {
    "key": "1234567890123456"
  },
  "result": "success",
  "resultData": {
    "cipher": true,
    "verify": "0A8F6FA7ACDF1224CF"
  }
}
```

После чего необходимо пройти второй этап авторизации, для этого нужно зашифровать данные `verify`, используя
приватный ключ в качестве ключа шифрования и ключ доступа в качестве вектора.

Пример кода для шифрования с помощью `crypto-js`

```js
cipherData (data) {
  return CryptoJS.AES.encrypt(data, CryptoJS.enc.Utf8.parse(this.private), {
    iv: CryptoJS.enc.Utf8.parse(this.key),
    mode: CryptoJS.mode.CBC
  })
}
```

Отправка авторизации для шифрования

```json
{
  "index": 1002,
  "command": "apiPrivateAuth",
  "data": {
    "verify": "BASE64 formatted string"
  }
}
```

Ответ:

```json
{
  "index": 1002,
  "command": "apiPrivateAuth",
  "data": {
    "verify": "BASE64 formatted string"
  }
  "result": "success",
  "resultData": {
    "level": 1,
    "cipher": true
  }
}
```

После этого сообщения, все запросы и ответы должны быть зашифрованы по аналогии с функцией выше,
только в качестве данных используется JSON запрос и ответ.

## Broadcast

В VRack некоторая информация может быть получена путем подписки на определенный канал.
Например, можно подписаться на канал дашборда `test` - `manager.test` и автоматически получать изменения состояния дашборда `test`.

Список каналов можно представить в виде дерева, например:

```
dashboards
|-test
  |-Device1
    |-render
|-terminal
  |-test2
    |-Device1
    |-error
```

Или плоского списка:

```
dashboards
dashboards.test
dashboards.test.Device1
dashboards.test.Device1.render
dashboards.test.Device1.terminal
dashboards.test2.Device1.error
```

Каждый раз, когда отправляется сообщение в канал, это же сообщение отправляется на канал более высокого уровня.
Если есть необходимость получать сообщения по всем дашбордам, всем устройствам и всем каналам устройств, можно подключиться к каналу `dashboards` (самый верх дерева).
Такой подход не очень эффективен, но в некоторых ситуациях это может быть полезно.

Для того чтобы отделить данные запросов от данных бродкастов, можно проверять параметр команды (broadcast).

Обычно бродкаст выглядит так:

```json
{
  "command": "broadcast",
  "channel": "dashboards.dashboard-name.Deviceid.render",
  "data": {},
  "target": "dashboards.dashboard-name.Deviceid.render"
}
```

Где:

- **command** _string_ - Обязательное поле команды
- **channel** _string_ - Канал, в который был отправлнен бродкаст
- **data** _object_ - Данные бродкаста
- **target** _string_ - Канал, на который был подписан клиент

Формат данных (data) определяет тот, кто отправляет данные в этот канал.

Например, данные, которые отправляет дашборд-менеджер при изменениях статуста дашборда:

```json
{
  "command": "broadcast",
  "channel": "manager.dashboard-id",
  "data": {
    "id": "dashboard-id",
    "name": "Dashboard name",
    "group": "GroupName",
    "run": false,
    "path": "dashboards/dashboard-id.json",
    "structurePath": "dashboards/dashboard-id.struct.json",
    "metaPath": "dashboards/dashboard-id.meta.json",
    "errors": 0,
    "deleted": false,
    "startedAt": 1615362665918
  },
  "target": "manager"
}
```

В примере выше можно обратить внимание на то, что "target" не соответствует "channel".
Дело в том, что в данном случае была подписка на канал "manager", но сообщение было отправлено
в канал "manager.dashboard-id". С помощью параметра "target" можно привязывать действие ко всем бродкастам,
происходящим в дочерних каналах.

Пример расширения вымышленного класса:

```js
class VRack {
  // ...
  channels = Map();
  // ...
  channelJoin(channel, cb) {
    // Подписываемся на канал
    const result = this.commandPromise("channelJoin", { data: { channel: channel } });
    // Сохраняем, что для этого канала CallBack cb
    this.channels.set(channel, cb);
    return result;
  }

  /**
   * Функция, которая вызывается, когда получен ответ от сервера
   *
   * @param {Object} data данные ответа от сервера
   */
  response(data) {
    // Смотрим наличие индекса
    if (data.index) {
      // Есть ли он на очереди
      if (this.queue.has(data.index)) {
        var func = this.queue.get(data.index);
        if (data.result === "error") {
          // Если вернулся ответ с ошибкой, вызываем reject
          func.reject(new Error(data.resultData));
        } else {
          // Если все ок, вызываем resolve
          func.resolve(data);
        }
        this.queue.delete(data.index);
      }
    } else if (data) {
      // Ищем именно по target наш CallBack
      if (this.channels.has(remoteData.target)) {
        // Получаем, запускаем
        this.channels.get(remoteData.target)(data);
      }
    }
  }
  // ...
}
```

### Каналы бродкастов VRack

#### manager

Получение информации об изменении дашбордов (включение, выключение, обновление и т.п.)

Структура:

```
manager
|-{dashboard-name} идентификатор дашборда
```

#### dashboards

Получение информации от устройств

Структура:

```
dashboards
|-{dashboard-name}    Идентификатор дашборда
  |-{DeviceName}      Идентификатор устройства
    |-render          Отправка shares данных
    |-terminal        Сообщение терминала
    |-notify          Сообщения нотификации
    |-error           Сообщение ошибок
    |-alert           Alert сообщения
    |-event           Канал событий устройства
    |-action          Результаты action запросов
```

## Дополнительно

На данный момент есть реализация работы с VRack на JS, но довольно сыроватая, хотя подходит в качестве примера:

- [VRackRemote](https://gitlab.com/vrack/vrackremote)

Приложение для мониторинга и управления VRack:

- [Интерфейс для работы с VRack](https://gitlab.com/vrack/dashboard-manager)
- [Консольный клиент для работы с VRack](https://gitlab.com/vrack/vrack-cli)

API для работы с VRack:

- [ApiKeyManager](/docs/api/ApiKeyManager.md) - Работа с ключами доступа, авторизация
- [DashboardManager](/docs/api/DashboardManager.md) - Управление дашбордами
- [Broadcaster](/docs/api/Broadcaster.md) - Работа с системой бродкастов
