# DeviceInformation (Plugin)

Предосталяет информацию о доступных устройствах на сервере

- LEVEL 3 [deviceListUpdate](#devicelistupdate) - Обновление списка устройств
- LEVEL 3 [deviceVendorList](#devicevendorlist) - Возвращает список вендоров устройств
- LEVEL 3 [deviceVendorInformation](#devicevendorinformation) - Возвращает информацию о вендоре и список устройств
- LEVEL 3 [deviceInformation](#deviceinformation) - Возвращает информацию об устройстве

## deviceListUpdate

Безопастно внутри воркера обновляет список устройств, формирует и возвращает основной список вендоров устройств.
Если не удалось сфорировать список, в `errors` будет сложено

### Возвращает

```js
[
  {
    name: 'basic',   // Имя вендора 
    dir: 'devices',  //  Директория вендора
    errors: ['list.json not found']       //  Список ошибок при обработке вендора
  }
]

```

## deviceVendorList

Возвращает список вендоров устройств без обновления списка устройств (кешированную версию)

### Возвращает

```js
[
  {
    name: 'basic',   // Имя вендора 
    dir: 'devices',  //  Директория вендора
    errors: ['list.json not found']       //  Список ошибок при обработке вендора
  }
]

```

## deviceVendorInformation

Возвращает информацию о вендоре и список устройств

### Параметры

- **vendor** _string_ - Идентификатор вендора

### Возвращает

```js
[
  {
    name: 'basic',   // Имя вендора 
    dir: 'devices',  //  Директория вендора
    errors: [],       //  Список ошибок при обработке вендора
    devices: [
      {
        errors: [],
        ​​​​name: "Aggregate"
      }
    ],
  }
]

```

### Исключения

- **CommandError** - Vendor not found


## deviceInformation

Возвращает информацию об устройстве

### Параметры

- **vendor** _string_ - Идентификатор вендора
- **device** _string_ - Идентификатор устройства

### Возвращает

```js
{
  name: 'Data',   // Имя вендора 
  errors: [],       //  Список ошибок при обработке вендора
  params: [
    // Массив экспортированных объектов параметров
  ],
  ports: [
    // Массив экспортированных объектов портов
  ]
}
```

### Исключения

- **CommandError** - Vendor not found
- **CommandError** - Device not found


