# Начало работы

 1. [Требования](#требования)
 2. [Установка](#установка)
 3. [Настройка автозапуска VRack в Ubuntu](#настройка-автозапуска-vrack-в-ubuntu)

## Требования
* nodejs версии 12  и выше. Желательно использовать nodejs новой версии. Проверить можно командой `node --version`.

## Установка

### Ubuntu/Linux/Mac

Выбираем место для установки, например **/opt** (рассматривается по умолчанию)
```
cd /opt/
```

Клонируем репозиторий
```
git clone https://gitlab.com/vrack/vrack.git
cd ./vrack
```

Устанавливаем зависимости через npm
```
npm install --only=production
```

В режиме разработки
```
npm install
```

Необходимо скопировать файлы конфигураций по умолчанию:

```
cp /opt/vrack/example.config.dashboard.json /opt/vrack/config.dashboard.json
cp /opt/vrack/example.config.devices.json /opt/vrack/config.devices.json
cp /opt/vrack/example.config.vrack.json /opt/vrack/config.vrack.json
```

Надо добавить хотя бы базовый набор устройств
```
mkdir ./storage
mkdir ./devices
cd ./devices
git clone https://gitlab.com/vrack/devices/basic.git
```

Уже можно приступить к запуску первого дашборда. Создаем папку с дашбордами
```
cd ../
mkdir ./dashboards
cd ./dashboards
```

Создаем файл с именем `test.json` и содержанием:

```json
{
    "devices": [
        {
            "id": "Interval",
            "type": "basic.Interval",
            "params": {
                "start": true,
                "timeout": 1200
            }
        },
        {
            "id": "Debug",
            "type": "basic.Debug",
            "params": {}
        }
    ],
    "connections": [
        "Interval.gate -> Debug.debug1"
    ]
}
```

Для автозапуска дашборда нам нужно определить файл с метаданными `test.meta.json`

```json
{
    "name" : "Тест",
    "group" : "Тест",
    "description" : "Наш первый тестовый дашборд",
    "autoStart": true,
    "autoReload": false,
    "version": "1.0"
}
```

И уже можно проверить работу нашего дашборда

```
node ./index.js
```

## Настройка автозапуска VRack в Ubuntu

Для автозапуска в ubuntu предусмотрен сервис файл. Копируем его

```
cp /opt/vrack/vrack.service /etc/systemd/system/
```

*Естественно, на свой страх и риск,* поскольку сервис запускается из под `root`. 

Обновляем демона сервисов

```
systemctl daemon-reload
systemctl enable vrack
systemctl start vrack
```